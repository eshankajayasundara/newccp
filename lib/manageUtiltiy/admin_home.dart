
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smart_hostel_management_system/manageUtiltiy/addUtilities.dart';
import 'package:smart_hostel_management_system/manageUtiltiy/add_utility.dart';
import 'package:smart_hostel_management_system/manageUtiltiy/updateUtilities.dart';

class adminHome extends StatefulWidget {
  @override
  _adminHomeState createState() => _adminHomeState();
}

class _adminHomeState extends State<adminHome> {
  Size screenSize;
  @override
  Widget build(BuildContext context) {

    screenSize = MediaQuery.of(context).size;
    return Scaffold(
      //backgroundColor: Colors.grey,
      appBar: new AppBar(
        backgroundColor: Colors.blue[900],
        title: new Text("Utility Management"),
        centerTitle: true,
        elevation: 0.1,
      ),
      endDrawer: new Container(
        width: screenSize.width - 50,
        color: Colors.white,
        child: new Drawer(
          child: new ListView(
            children: <Widget>[
              //      header
              new UserAccountsDrawerHeader(
                accountName: Text('App Admin'),
                accountEmail: Text('support@gmail.com'),
                currentAccountPicture: GestureDetector(
                  child: new CircleAvatar(
                    backgroundColor: Colors.white,
                    child: Icon(Icons.person, color: Colors.blue[900],),
                  ),
                ),
                decoration: new BoxDecoration(
                    color: Colors.blue[900]
                ),
              ),
              //          body

              InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => new adminHome()));
                },
                child: ListTile(
                  title: Text('Home Page'),
                  leading: Icon(Icons.home,color: Colors.red, size: 30.0,),
                ),
              ),

              InkWell(
                onTap: (){},
                child: ListTile(
                  title: Text('My Account'),
                  leading: Icon(Icons.person,color: Colors.black, size: 30.0,),
                ),
              ),

              InkWell(
                onTap: (){},
                child: ListTile(
                  title: Text('Add Utilities'),
                  leading: Icon(Icons.add,color: Colors.blueAccent,size: 30.0),
                ),
              ),

              InkWell(
                onTap: (){
                  // Navigator.push(context, MaterialPageRoute(builder: (context) => new UtilityCart()));
                },
                child: ListTile(
                  title: Text('Update Utilities',),
                  leading: Icon(Icons.update,color: Colors.red,size: 30.0),
                ),
              ),

              Divider(),
              InkWell(
                onTap: (){},
                child: ListTile(
                  title: Text('Delete Utilities'),
                  leading: Icon(Icons.delete,color: Colors.black,size: 30.0),
                ),
              ),

              InkWell(
                onTap: (){},
                child: ListTile(
                  title: Text('Setting'),
                  leading: Icon(Icons.settings, color: Colors.green,size: 30.0),
                ),
              )
            ],
          ),
        ),
      ),
      body: new Column(

                children: <Widget>[
          new SizedBox(

            height: 10.0,
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                GestureDetector(
                 onTap:(){
                   Navigator.of(context).push(new CupertinoPageRoute(builder: (context) => addUtility()));
                  // Navigator.of(context).push(new CupertinoPageRoute(builder: (context) => add_Utilities()));
                 },
                  child: new CircleAvatar(

                    backgroundColor: Colors.grey,
                    maxRadius: 90.0,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Icon(Icons.add ,size: 50.0,),
                        new SizedBox(
                        height: 10.0,
                        ),
                        new Text('Add Utilities',style: TextStyle(color: Colors.black),)
                      ],
                    ),
                  ),
                ),

              ],
            ),
          ),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        GestureDetector(
                          onTap:(){
                            // Navigator.of(context).push(new CupertinoPageRoute(builder: (context) => addUtility()));
                            Navigator.of(context).push(new CupertinoPageRoute(builder: (context) => updateUtility()));
                          },

                          child: new CircleAvatar(

                            backgroundColor: Colors.grey,
                            maxRadius: 90.0,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Icon(Icons.update ,size: 50.0,),
                                new SizedBox(
                                  height: 10.0,
                                ),
                                new Text('Update Utilities',style: TextStyle(color: Colors.black))
                              ],
                            ),
                          ),
                        ),

                      ],
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        GestureDetector(
                          onTap:(){
                            // Navigator.of(context).push(new CupertinoPageRoute(builder: (context) => addUtility()));
                            Navigator.of(context).push(new CupertinoPageRoute(builder: (context) => add_Utilities()));
                          },
                          child: new CircleAvatar(

                            backgroundColor: Colors.grey,
                            maxRadius: 90.0,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Icon(Icons.delete_forever ,size: 50.0,),
                                new SizedBox(
                                  height: 10.0,
                                ),
                                new Text('Delete Utilities',style: TextStyle(color: Colors.black))
                              ],
                            ),
                          ),
                        ),

                      ],
                    ),
                  ),






        ],
      ),
    );
  }
}
