import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:smart_hostel_management_system/widgets/utilityProducts.dart';
import 'package:smart_hostel_management_system/Student/Home_Body.dart';
import 'package:smart_hostel_management_system/Student/cart_screen.dart';

class StudentHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Utility Facilities"),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.shopping_cart,size: 30,), onPressed: () => Navigator.of(context).pushNamed(CartScreen.routeName),)
        ],


      ),

      drawer: Drawer(),
      body: HomeBody(),



    );
  }
}
