import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smart_hostel_management_system/Hostel/cart.dart';
import 'package:smart_hostel_management_system/Hostel/utilityDetails.dart';
import 'package:smart_hostel_management_system/manageUtiltiy/admin_home.dart';

import 'package:smart_hostel_management_system/tools/Store.dart';


class mainHostelPage extends StatefulWidget {
  @override
  _mainHostelPageState createState() => _mainHostelPageState();
}

class _mainHostelPageState extends State<mainHostelPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.blue[900],
        centerTitle: true,
        title: Text("Utility Facilities"),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.shopping_cart,size: 30,),
            onPressed: (){}
          )
        ],

      ),

      body: new Center(
        child: new Column(
        children: <Widget>[
          new Flexible(
            child: new GridView.builder(
              gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
    crossAxisCount: 2),
    itemCount: storeItems.length,
    itemBuilder: (BuildContext context, int index){
            return GestureDetector(
              onTap: (){
                Navigator.of(context).push(new MaterialPageRoute(builder: (context) => new ItemDetail.items(
                  itemImage:storeItems[index].itemImage,
                  itemPrice:storeItems[index].itemPrice,
                  itemName:storeItems[index].itemName,
                  itemRating:storeItems[index].itemRating,
                )));
              },
              child: new Card(
                child: Stack(
                  alignment: FractionalOffset.topLeft,
                  children: <Widget>[
                    new Stack(
                        alignment: FractionalOffset.bottomCenter,
                        children: <Widget>[
                          new Container(
                            decoration: new BoxDecoration(

                              image: new DecorationImage(
                               // fit: BoxFit.fill,
                                  image: new AssetImage(
                                storeItems[index].itemImage
                              ))
                            ),

                          ),
                          new Container(
         height: 35.0,
         color: Colors.black.withAlpha(100),
         child:  new Padding(
           padding: const EdgeInsets.all(8.0),
           child: new Row(
               mainAxisAlignment: MainAxisAlignment.spaceBetween,
               children: <Widget>[
                     new Text("${storeItems[index].itemName.substring(0,3)}...",
                         style: new TextStyle(fontWeight: FontWeight.bold,fontSize: 16.0,color: Colors.white)),
                     new Text("Rs.${storeItems[index].itemPrice}",
                       style: new TextStyle(color: Colors.red[500],fontWeight: FontWeight.w700),),

               ],
           ),
         ),
       )
          ],
          ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Container(
                          height: 30.0,
                          width: 60.0,
                          decoration: new BoxDecoration(
                            color: Colors.black,
                            borderRadius: new BorderRadius.only(
                              topRight: new Radius.circular(5.0),
                                bottomRight: new Radius.circular(5.0))
                          ),
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              new Icon(Icons.star,color: Colors.white,
                              size: 20.0,),
                              new Text("${storeItems[index].itemRating}",style: new TextStyle(color: Colors.white),)
                            ],
                          ),
                        ),
                        new IconButton(icon: Icon(Icons.add_shopping_cart, color: Colors.black,),
                            onPressed: (){})
                      ],
                    )
                  ],
                ),
               ),
            );

              }

              ))

              ],
              ),
        ),



      
      floatingActionButton: Stack(
        alignment: Alignment.topLeft,
        children: <Widget>[
          new FloatingActionButton(
              onPressed: (){
                Navigator.of(context).push(new CupertinoPageRoute(builder: (BuildContext context) => new Cart()));},
            child: new Icon(Icons.shopping_cart),

          ),
          new CircleAvatar(
            radius: 10.0,
            backgroundColor: Colors.red,
            child: new Text('0'),
          )
        ],
      ),

      drawer: new Drawer(
        child: new Column(
          children: <Widget>[
            //      header
            new UserAccountsDrawerHeader(
              accountName: Text('Thamashi Kotuwegedara'),
              accountEmail: Text('thamashi97@gmail.com'),
              currentAccountPicture: GestureDetector(
                child: new CircleAvatar(
                  backgroundColor: Colors.grey,
                  child: Icon(Icons.person, color: Colors.white,),
                ),
              ),
              decoration: new BoxDecoration(
                  color: Colors.blue[900]
              ),
            ),
            //          body

            InkWell(
              onTap: (){
               Navigator.push(context, MaterialPageRoute(builder: (context) => new adminHome()));
              },
              child: ListTile(
                title: Text('Home Page'),
                leading: Icon(Icons.home,color: Colors.red,),
              ),
            ),

            InkWell(
              onTap: (){},
              child: ListTile(
                title: Text('My Account'),
                leading: Icon(Icons.person,color: Colors.black,),
              ),
            ),

            InkWell(
              onTap: (){},
              child: ListTile(
                title: Text('My Orders'),
                leading: Icon(Icons.shopping_basket,color: Colors.blue),
              ),
            ),

            InkWell(
              onTap: (){
               // Navigator.push(context, MaterialPageRoute(builder: (context) => new Cart()));
              },
              child: ListTile(
                title: Text('Shopping Cart'),
                leading: Icon(Icons.shopping_cart,color: Colors.blue),
              ),
            ),

            Divider(),
            InkWell(
              onTap: (){},
              child: ListTile(
                title: Text('Settings'),
                leading: Icon(Icons.settings,color: Colors.black,),
              ),
            ),

            InkWell(
              onTap: (){},
              child: ListTile(
                title: Text('About'),
                leading: Icon(Icons.help, color: Colors.green,),
              ),
            )
          ],
        ),
      ),
    );

  }
}
