import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:smart_hostel_management_system/widgets/utility_models.dart';
import 'package:smart_hostel_management_system/widgets/ult_items.dart';

class AllUtilities extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //product count
    final utilityData = Provider.of<UtilityItems>(context);
    final uts = utilityData.items;

    return GridView.builder(
      physics: ScrollPhysics(),
        shrinkWrap: true,
        itemCount: uts.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemBuilder: (context, i) => ChangeNotifierProvider.value(value: uts[i],
        child: Utl_Items(name: uts[i].utility_name,img: uts[i].img,),));
  }
}
