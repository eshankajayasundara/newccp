import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class utilities with ChangeNotifier{
  final String utility_id;
  final String utility_name;
  final String description;
  final String brand;
  final String img;
  final double price;

  utilities({
    @required this.utility_id,
    @required this.utility_name,
    @required this.description,
    @required this.brand,
    @required this.img,
    @required this.price
  });

}


class UtilityItems with ChangeNotifier{
  List <utilities> _items = [
    utilities(
        utility_id: '1',
        utility_name: 'washing Machine',
        description: 'This is a refiebbbx hdhdhdh hdhdhhdhd',
        brand: 'LG',
        img: 'images/laundry.png',
        price: 250,
    ),

    utilities(
      utility_id: '2',
      utility_name: 'Iron',
      description: 'This is a refiebbbx hdhdhdh hdhdhhdhd',
      brand: 'LG',
      img: 'images/household-appliance.png',
      price: 250,
    ),

    utilities(
      utility_id: '3',
      utility_name: 'Heater',
      description: 'This is a refiebbbx hdhdhdh hdhdhhdhd',
      brand: 'LG',
      img: 'images/electric-heater.png',
      price: 250,
    ),
    utilities(
      utility_id: '4',
      utility_name: 'Fan',
      description: 'This is a refiebbbx hdhdhdh hdhdhhdhd',
      brand: 'LG',
      img: 'images/fan.png',
      price: 250,
    ),
    utilities(
      utility_id: '5',
      utility_name: 'Rice Cooker',
      description: 'This is a refiebbbx hdhdhdh hdhdhhdhd',
      brand: 'LG',
      img: 'images/rice-cooker.png',
      price: 250,
    ),
    utilities(
      utility_id: '6',
      utility_name: 'Kettel',
      description: 'This is a refiebbbx hdhdhdh hdhdhhdhd',
      brand: 'LG',
      img: 'images/electric-teapot.png',
      price: 250,
    ),

  ];

  //getting the items

 List <utilities> get items{
  return[... _items];
}

  utilities findById(String utilityID){
   return _items.firstWhere((utl) => utl.utility_id == utilityID);
}
}