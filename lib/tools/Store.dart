import 'package:flutter/material.dart';

class Store{
  String itemName;
  double itemPrice;
  String itemImage;
  double itemRating;

  Store.items({
    this.itemName,
    this.itemPrice,
    this.itemImage,
    this.itemRating
});

}

List<Store> storeItems = [
  Store.items(
    itemName: "Washing machine",
    itemPrice: 547.00,
    itemImage: "images/household-appliance.png",
    itemRating: 0.0
  ),

  Store.items(
      itemName: "Heater",
      itemPrice: 220.00,
      itemImage: "images/electric-heater.png",
      itemRating: 0.0
  ),


  Store.items(
      itemName: "Fan",
      itemPrice: 500.00,
      itemImage: "images/fan.png",
      itemRating: 0.0
  ),

  Store.items(
      itemName: "Kettel",
      itemPrice: 250.00,
      itemImage: "images/electric-teapot.png",
      itemRating: 0.0
  ),

  Store.items(
      itemName: "Rice Cooker",
      itemPrice: 250.00,
      itemImage: "images/rice-cooker.png",
      itemRating: 0.0
  ),

  Store.items(
      itemName: "Washing machine",
      itemPrice: 200.00,
      itemImage: "images/laundry.png",
      itemRating: 0.0
  ),

  Store.items(
      itemName: "Rice Cooker",
      itemPrice: 250.00,
      itemImage: "images/rice-cooker.png",
      itemRating: 0.0
  ),

  Store.items(
      itemName: "Washing machine",
      itemPrice: 200.00,
      itemImage: "images/laundry.png",
      itemRating: 0.0
  )

];