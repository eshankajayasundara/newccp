import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
//import 'package:payment/owner/received_payment.dart';
import 'package:smart_hostel_management_system/Payment/owner/received_payment.dart';

class PaymentHistory extends StatefulWidget {

  @override
  _PaymentHistory createState() => _PaymentHistory();
}

class _PaymentHistory extends State<PaymentHistory> {
  int _act = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Payment History"),
      ),
      body: Container(
        child: ListView(
          padding: EdgeInsets.only(bottom: 100.0),
          children: <Widget>[
            Container(
              padding: new EdgeInsets.all(5.0),
            ),
            Container(
              height: 70.0,
              child: Card(
                elevation: 5.0,
                shadowColor: Color(0xFF0d47a1),
//              shadowColor: Colors.redAccent,
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundImage: NetworkImage("https://www.woolha.com/media/2020/03/eevee.png"),
                    radius: 25.0,
                  ),
//                  contentPadding: EdgeInsets.only(top: 10.0),
                  title: Text('Lakpriya'),
//                trailing: Icon(Icons.more_vert),
                    trailing: Text("View Payment"),
                ),
              ),
            ),
            Container(
              padding: new EdgeInsets.all(5.0),
            ),
            Container(
              height: 70.0,
              child: Card(
                elevation: 5.0,
                shadowColor: Color(0xFF0d47a1),
                child: ListTile(
//                leading: FlutterLogo(size: 56.0),
                  leading: CircleAvatar(
                    backgroundImage: NetworkImage("https://www.woolha.com/media/2020/03/eevee.png"),
                    radius: 25.0,
                  ),
                  title: Text('Weerakoon'),
//                subtitle: Text('Here is a second line'),
//                trailing: Icon(Icons.more_vert),
                  trailing:Text("View Payment"),
                ),
              ),
            ),
            Container(
              padding: new EdgeInsets.all(5.0),
            ),
            Container(
              height: 70.0,
              child: Card(
                elevation: 5.0,
                shadowColor: Color(0xFF0d47a1),
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundImage: NetworkImage("https://www.woolha.com/media/2020/03/eevee.png"),
                    radius: 25.0,
                  ),
                  title: Text('Maheel'),
//                subtitle: Text('Here is a second line'),
                  trailing:  Text("View Payment"),
                  onTap: (){
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (_){
                          return ReceivedPayment();
                        }
                    ),
                    );
                  },
                ),
              ),
            ),
            Container(
              padding: new EdgeInsets.all(5.0),
            ),
            Container(
              height: 70.0,
              child: Card(
                elevation: 5.0,
                shadowColor: Color(0xFF0d47a1),
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundImage: NetworkImage("https://www.woolha.com/media/2020/03/eevee.png"),
                    radius: 25.0,
                  ),
                  title: Text('Dewapriya'),
//                subtitle: Text('Here is a second line'),
//                trailing: Icon(Icons.more_vert),
                  trailing: Text("View Payment"),
                ),
              ),
            ),
            Container(
              padding: new EdgeInsets.all(5.0),
            ),
            Container(
              height: 70.0,
              child: Card(
                elevation: 5.0,
                shadowColor: Color(0xFF0d47a1),
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundImage: NetworkImage("https://www.woolha.com/media/2020/03/eevee.png"),
                    radius: 25.0,
                  ),
                  title: Text('Malintha'),
//                subtitle: Text('Here is a second line'),
                  trailing:  Text("View Payment"),
                  onTap: (){},
                ),
              ),
            ),
            Container(
              padding: new EdgeInsets.all(5.0),
            ),
            Container(
              height: 70.0,
              child: Card(
                elevation: 5.0,
                shadowColor: Color(0xFF0d47a1),
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundImage: NetworkImage("https://www.woolha.com/media/2020/03/eevee.png"),
                    radius: 25.0,
                  ),
                  title: Text('Fernando'),
//                subtitle: Text('Here is a second line'),
//                trailing: Icon(Icons.more_vert),
                  trailing: Text("View Payment"),
                ),
              ),
            ),
            Container(
              padding: new EdgeInsets.all(5.0),
            ),
            Container(
              height: 70.0,
              child: Card(
                elevation: 5.0,
                shadowColor: Color(0xFF0d47a1),
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundImage: NetworkImage("https://www.woolha.com/media/2020/03/eevee.png"),
                    radius: 25.0,
                  ),
                  title: Text('Chamuditha'),
//                subtitle: Text('Here is a second line'),
                  trailing:  Text("View Payment"),
                  onTap: (){},
                ),
              ),
            ),
            Container(
              padding: new EdgeInsets.all(5.0),
            ),
            Container(
              height: 70.0,
              child: Card(
                elevation: 5.0,
                shadowColor: Color(0xFF0d47a1),
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundImage: NetworkImage("https://www.woolha.com/media/2020/03/eevee.png"),
                    radius: 25.0,
                  ),
                  title: Text('Yasith'),
//                subtitle: Text('Here is a second line'),
//                trailing: Icon(Icons.more_vert),
                  trailing: Text("View Payment"),
                ),
              ),
            ),
            Container(
              padding: new EdgeInsets.all(5.0),
            ),
            Container(
              height: 70.0,
              child: Card(
                elevation: 5.0,
                shadowColor: Color(0xFF0d47a1),
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundImage: NetworkImage("https://www.woolha.com/media/2020/03/eevee.png"),
                    radius: 25.0,
                  ),
                  title: Text('Kataz'),
//                subtitle: Text('Here is a second line'),
                  trailing:  Text("View Payment"),
                  onTap: (){},
                ),
              ),
            ),
            Container(
              padding: new EdgeInsets.all(5.0),
            ),
          ],
        ),
      ),
//      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,// This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}