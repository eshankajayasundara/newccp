import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
//import 'package:payment/student/payment_options.dart';
import 'package:smart_hostel_management_system/Payment/student/payment_options.dart';
import 'home_page.dart';

class PhotoUpload extends StatefulWidget {
  @override
  _PhotoUploadState createState() => _PhotoUploadState();
}

class _PhotoUploadState extends State<PhotoUpload> {

  File _image;

  Future getImage() async{
    final image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      _image = image;
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Bank Slip"),
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: (){
              Navigator.pop(context,PaymentOptions());
            }
        ),
        actions: <Widget>[
          IconButton(
//            padding:EdgeInsets.only(right: 30.0),
              icon: Icon(Icons.home),
              onPressed: (){
//                Navigator.of(context)..push(MaterialPageRoute(
//                    builder: (_){
//                      return MyHomePage();
//                    }));
              }
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [
            Container(
//                height: double.infinity,
//                width: double.infinity,
//                padding: EdgeInsets.only(bottom: 50.0),

                child: _image == null ?
                Text(
                    "Upload the Bank Slip"
                ):
                    Image.file(_image)
            ),
            Container(
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: getImage,
        tooltip: "Upload a Photo",
        child: Icon(Icons.photo_camera),
      ),
    );
  }
}
