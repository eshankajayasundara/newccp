import 'package:flutter/material.dart';
import 'student/home_page.dart';
import 'student/payment_options.dart';

//const PrimaryColor = const Color(0xFF0d47a1);

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
//        primarySwatch: Colors.cyan,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        appBarTheme: AppBarTheme(
          color: Color(0xFF0d47a1),
        )
      ),
      home: MyHomePage(title: 'Payment Home Page'),
//      home: PaymentOptions(),
    );
  }
}

