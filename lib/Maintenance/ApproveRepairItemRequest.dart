import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ApproveRepairItem extends StatefulWidget {
  @override
  _ApproveRepairItemState createState() => _ApproveRepairItemState();
}

class _ApproveRepairItemState extends State<ApproveRepairItem> {
  @override
  Widget build(BuildContext context) {
    var _controll;
    return new MaterialApp(
      home: new Scaffold(

        appBar: AppBar(
          title: new Center(child: Text('ITEM DETAILS')),
        ),

        body: Container(
          child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,


            children: <Widget>[


              SizedBox(
                height: 20,
              ),

              Text("photo",
              ),

              SizedBox(
                height: 100,
              ),

              TextField(
                controller: _controll,
                decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                        borderSide:BorderSide(color: Colors.lightBlue),
                        borderRadius: BorderRadius.all(Radius.circular(30))
                    ),
                ),
              ),

              SizedBox(
                height: 100,
              ),

              Text("Description"),

              SizedBox(
                height: 40,
              ),

              TextField(
                controller: _controll,
                decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderSide:BorderSide(color: Colors.lightBlue),
                      borderRadius: BorderRadius.all(Radius.circular(30))
                  ),
                ),
              ),

              SizedBox(
                height: 50,
              ),

              Text("Location"),

              SizedBox(
                height: 40,
              ),

              TextField(
                controller: _controll,
                decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderSide:BorderSide(color: Colors.lightBlue),
                      borderRadius: BorderRadius.all(Radius.circular(30))
                  ),
                ),
              ),

              SizedBox(
                height: 40,
              ),

              Center(
                child: FlatButton(
                  onPressed: (){},
                  color: Colors.blue,
                  child: Text("APPROVE"),
                ),
              )
            ],
          ),
          ),
        ),
      ),
    );




  }
}
