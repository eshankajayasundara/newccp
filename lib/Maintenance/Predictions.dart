import 'package:flutter/material.dart';

class Predictions extends StatefulWidget {
  @override
  _PredictionsState createState() => _PredictionsState();
}

class _PredictionsState extends State<Predictions> {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        appBar: AppBar(
          title: Text('PREDICTIONS'),
          centerTitle: true,
        ),
        body: Container(
          child: Column(
            children: <Widget>[

              SizedBox(
                width: 400.0,
                height: 50.0,
              ),
              MaterialButton(

                splashColor: Colors.red,
                color: Colors.blue,
                onPressed: () {},
                child: Text(
                  "DISPLAY THE PREDICTION LIST",
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
