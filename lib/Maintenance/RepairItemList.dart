import 'package:flutter/material.dart';


import 'MakeRepairRequest2.dart';


class home extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return new Scaffold(

      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context){
            return  MakeRepairRequest();
          }));
        },
      ),

      appBar: new AppBar(
        title: Text('REPAIR ITEM LIST'),
        centerTitle: true,
      ),

      body: new ListView(
        children: <Widget>[


          new Container(
            padding: new EdgeInsets.all(20.0),
            child: new Center(
              child: new Column(
                children: <Widget>[
                  new item(Item: 'images/tap.png',),
                  new item(Item: 'images/door-lock.png',),
                  new item(Item: 'images/fan79.png',),
                  new item(Item: 'images/bulb.png',),

                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}



class item extends StatelessWidget{
  final String Item;

  const item({Key key, this.Item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Container(
        padding: new EdgeInsets.all(20.0),
        child: new Center(
          child: new Row(
              children: <Widget>[
                new Image(
                  image: new AssetImage(Item),
                  width: 100.0,
                ),
              ]
          ),

        )


    );

  }

}

