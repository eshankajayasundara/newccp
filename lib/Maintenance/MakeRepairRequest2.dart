
import 'dart:io';

import 'package:dropdownfield/dropdownfield.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:smart_hostel_management_system/Maintenance/RepairItemList.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


import 'Items.dart';


class MakeRepairRequest extends StatefulWidget {

  File photo;

  MakeRepairRequest({this.photo});

  @override
  _MakeRepairRequestState createState() => _MakeRepairRequestState();
}



class _MakeRepairRequestState extends State<MakeRepairRequest> {


  File photo;


  Future getImage() async {
    // ignore: deprecated_member_use
    var tempImage = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      photo = tempImage;
    });

  }



  picker() async {
    print('Picker is called');
    // ignore: deprecated_member_use
    File img = await ImagePicker.pickImage(source: ImageSource.camera);
    photo = img;
    print(img.path);
    setState(() {});
    GestureDetector(
      onTap: () =>
          Navigator.of(context).push(new MaterialPageRoute(
              builder: (context) => new MakeRepairRequest())),
    );
  }


  TextEditingController TypeController = TextEditingController();
  TextEditingController locationController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController locationDetailsController = TextEditingController();


  bool isEditing = false;
  bool textFieldVisibility = false;

  String firestoreCollectionName = "Repair_items";
  items currentitems;

  getAllRepair_items() {
    return Firestore.instance.collection(firestoreCollectionName).snapshots();

  }


  addItemDetails() async {
    items itemDetails = items(
        Type: TypeController.text,
        location: locationController.text,
        description: descriptionController.text,
        locationDetails: locationDetailsController.text);


    try {
      Firestore.instance.runTransaction(
              (Transaction transaction) async {
            await Firestore.instance
                .collection(firestoreCollectionName)
                .document()
                .setData(itemDetails.toJson());
          }
      );
    } catch (e) {
      print(e.toString());
    }
  }


  Widget build(BuildContext context) {
    var _controller;


    return new MaterialApp(
      home: new Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          title: new Center(child: Text('MAINTENANCE MANAGEMENT')),
        ),


        body: Container(
            height: MediaQuery
                .of(context)
                .size
                .height,
            width: MediaQuery
                .of(context)
                .size
                .height,
            padding: EdgeInsets.all(20.0),
            margin: EdgeInsets.fromLTRB(10.0, 20.0, 30.0, 40.0),

            child: SingleChildScrollView(

              child: Column(

                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,


                children: <Widget>[

                  Text(
                    "Image",
                    textAlign: TextAlign.left,
                  ),

                  Center(child: photo == null
                      ? new Text('No Any Image is Selected')
                      : new Image.file(photo),),

                  SizedBox(height: 30.0,),


                  Text(
                    "Type",
                    textAlign: TextAlign.left,

                  ),


                  SizedBox(height: 40.0,),


                  DropDownField(
                    controller: TypeController,
                    hintText: 'Select A Type',
                    enabled: true,
                    items: types,
                    onValueChanged: (value) {
                      setState(() {
                        selectType = value;
                      });
                    },
                  ),


                  SizedBox(
                    height: 40.0,
                  ),


                  Text(
                    "Location",
                    textAlign: TextAlign.left,
                  ),


                  SizedBox(
                    height: 40.0,
                  ),


                  DropDownField(
                    controller: locationController,
                    hintText: 'Select A Location',
                    enabled: true,
                    items: type,
                    onValueChanged: (value) {
                      setState(() {
                        selectLocation = value;
                      });
                    },

                  ),


                  SizedBox(
                    height: 40.0,
                  ),


                  TextField(
                    controller: descriptionController,
                    decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.lightBlue),
                            borderRadius: BorderRadius.all(Radius.circular(30))
                        ),

                        hintText: 'The Description Of The Item'
                    ),
                  ),


                  SizedBox(
                    height: 40.0,
                  ),


                  TextField(
                    controller: locationDetailsController,
                    decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.lightBlue),
                            borderRadius: BorderRadius.all(Radius.circular(30))

                        ),

                        hintText: 'Please enter the Location'
                    ),
                  ),


                  SizedBox(
                    height: 40.0,
                  ),


                  MaterialButton(
                    splashColor: Colors.red,
                    color: Colors.blue,
                    onPressed: () {
                      addItemDetails();
                      getImage();

                      final StorageReference firebaseStorageRef = FirebaseStorage.instance.ref().child('photo.jpg');
                      final StorageUploadTask task = firebaseStorageRef.putFile(photo);


                      setState(() {
                        textFieldVisibility = true;
                      });
                    },
                    child: Text(
                      "CONFIRM",
                    ),
                  ),


                  SizedBox(
                    height: 40.0,
                  ),


                ],
              ),
            )
        ),
        floatingActionButton: new FloatingActionButton(
            onPressed: picker,
            child: new Icon(Icons.add_a_photo)
        ),
      ),
    );
  }


// ignore: camel_case_types


  String selectType = "";
  final typesSelected = TextEditingController();

  List<String> types = [
    "Electricity",
    "Water",
  ];


  String selectLocation = "";

// ignore: non_constant_identifier_names
  final LocationSelected = TextEditingController();

  List<String> type = [
    "First Floor",
    "Second Floor",
    "Third Floor",
  ];

}
