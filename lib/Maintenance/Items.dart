import 'package:cloud_firestore/cloud_firestore.dart';

class items{
  // ignore: non_constant_identifier_names


  String Type;
  String location;
  String description;
  String locationDetails;
  String image;


  DocumentReference documentReference;
  //this.Type, this.location,
  // ignore: non_constant_identifier_names
  items({this.Type, this.location,this.description, this.locationDetails, this.image});

  items.fromMap(Map<String,dynamic> map, {this.documentReference}){
    Type = map["Type"];
    location = map["location"];
    description = map["description"];
    locationDetails = map["locationDetails"];
    image = map["image"];
  }

  items.fromSnapshot(DocumentSnapshot snapshot)
      :this.fromMap(snapshot.data, documentReference:snapshot.reference);

  toJson(){
    return{'Type' : Type, 'location' : location, 'description' : description, 'locationDetails' : locationDetails, 'image' : image};
  }
}